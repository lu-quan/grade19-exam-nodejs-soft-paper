'use strict';

let nunjucks=require('nunjucks');

function createEnv(path,opts){
    path=path || 'views';
    opts=opts || {};
    let confOpts={
        autoescape:opts.autoescape || true,
        throwOnUndefined:opts.throwOnUndefined ||false,
        trimBlocks:opts.trimBlocks || false,
        lstripBlocks:opts.lstripBlocks || true,
        watch:opts.watch || true,
        noCache:opts || true
    }
    let env=nunjucks.configure(path,confOpts);
    return env;

}

module.exports=async(ctx,next)=>{
    ctx.render=function(view,model){
        let env=createEnv();
        ctx.body=env.render(view,model);
    }
    await next();
}