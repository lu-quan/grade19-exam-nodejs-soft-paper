'use strict';
//引入模块
let fs=require('fs');
let path=require('path');
let router=require('koa-router')();



//搜索
function searchCon(dir){
    let file =fs.readdirSync(dir);

    let fileCon=file.filter((name)=>{
        return name.endsWith('.js') && name !== 'index.js';
    })
    return fileCon;
}


//注册
function registerCon(file){
    file.forEach(item => {
        let tempPath=path.join(__dirname,item);

        let route=require(tempPath);
        for(let r in route){
            let type=route[r][0];
            let fn=route[r][1];
            if(type==='get'){
                router.get(r,fn);
            }else if(type==='post'){
                router.post(r,fn);
            }else if(type==='delete'){
                router.delete(r,fn);
            }else if(type==='put'){
                router.put(r,fn);
            }
        }
    });
}




//暴露
module.exports=function(Dir){
    let dir=Dir || __dirname;

    let a=searchCon(dir);

    registerCon(a);

    return router.routes();
}