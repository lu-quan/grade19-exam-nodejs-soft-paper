'use strict';

let Koa=require('koa');
let router=require('koa-router')();
let bodyParser=require('koa-bodyparser');
let Cons=require('./controllers')
let temp=require('./templating')
let staticRes=require('koa-static');

let app=new Koa();

app.use(bodyParser());
app.use(Cons());
app.use(temp);

//静态资源处理
app.use(staticRes(__dirname+'/static'));

app.use(async(ctx,next)=>{
    ctx.render('index.html');
})

let port =8000;
app.listen(port);
console.log(`http://localhost:${port}`);